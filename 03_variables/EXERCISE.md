
- What are two ways to create a new variable?
- What is the value of x after running x := 5; x += 1?
- What is scope? How do you determine the scope of a variable in Go?
- What is the difference between var and const?
- Using the example program as a starting point, write a program that converts from Fahrenheit into Celsius (C = (F − 32) * 5/9).
- Write another program that converts from feet into meters (1 ft = 0.3048 m).

[Go Back](./README.md)