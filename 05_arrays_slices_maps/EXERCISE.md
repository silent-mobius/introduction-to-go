

- How do you access the fourth element of an array or slice?
- What is the length of a slice created using make([]int, 3, 9)?
- Given the following array, what would x[2:5] give you?
```go
x := [6]string{"a","b","c","d","e","f"}
```
Write a program that finds the smallest number in this list:
```go
x := []int{
    48,96,86,68,
    57,82,63,70,
    37,34,83,27,
    19,97, 9,17,
}
```
