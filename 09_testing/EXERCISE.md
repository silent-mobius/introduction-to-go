

- Writing a good suite of tests is not always easy, but the process of writing tests often reveals more about a problem than you may at first realize. For example, with our Average function, what happens if you pass in an empty list ([]float64{})? How could the function be modified to return 0 in this case?
- Write a series of tests for the Min and Max functions you wrote in the previous chapter