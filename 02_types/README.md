
---

# Types

---

## Numbers

#### Integers

#### Floats

---

## Strings


---

## Boolean



---

## Operators

#### Number Operators

#### String Operators

#### Boolean Operators


---

## Rune


---

##  Byte


---

## Uintptr

---

## Exercises

[Here](EXERCISE.md)