

- Why do we use packages?
- What is the difference between an identifier that starts with a capital letter and one that doesn’t (e.g., Average versus average)?
- What is a package alias? How do you make one?
- We copied the average function from Chapter 6 to our new package. Create Min and Max functions that find the minimum and maximum values in a slice of float64s.
- How would you document the functions you created in #4?
