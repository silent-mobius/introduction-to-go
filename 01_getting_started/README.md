# Getting Started

---

## Getting Started

Go is a general-purpose programming language with advanced features and a clean syntax. Because of its wide availability on a variety of platforms, its robust well-documented common library, and its focus on good software engineering principles, Go is a great programming language to learn.


---

## Who is this course for ?

Although this book is suitable for inexperienced programmers, if you have never programmed before you will probably find the material too difficult to follow. You may benefit from consulting a more general programming resource before diving into the material here, but in all honesty, most students need the kind of hands-on, personal support that you might find in a classroom setting or one on one with an experienced developer.

---

## Environment Setup

- Go compiler install
- Development tools:
    - Text editor:
        - Vim
        - VScode
        - GoLand
    - Version control:
        - Git is MUST
    - Terminal:
        - on windows: Git-Bash, CMD, PowerShell


---

## Testing Environment

- Go Compiler
    - version
    - path
    - help

---

## Hello World

#### Project Structure

```sh
mkdir -p src/go-basics/ch1
cd src/go-basics/ch1
vim main.go
```

#### Hello World


```go
package main

import "fmt"

// this is a comment

func main() {
    fmt.Println("Hello, World")
}
```

- why all languages provide hello-world example ? to show basic scructure and project initialization.
    - we'll practice this additionally later on

#### Run Code

```sh
go run main.go
go build main.go
./main 

```

---

## Explaining Hello World

```go
package main
```

```go
import "fmt"
```

```go
// this is a comment
```

```go
func main() {
    fmt.Println("Hello, World")
}
```

---

## GoDoc

```sh
godoc fmt Println
```

---
## Exercise

[Here](./EXERCISE.md)