
- What is whitespace?
- What is a comment? What are the two ways of writing a comment?
- Our program began with package main. What would the files in the fmt package begin with?
- We used the `Println` function defined in the `fmt` package. If you wanted to use the `Exit` function from the `os` package, what would you need to do?
- Modify the program we wrote so that instead of printing `Hello, World` it prints `Hello, my name is` followed by your name.

[Go Back](./README.md)