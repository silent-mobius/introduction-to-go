

- What’s the difference between a method and a function?
- Why would you use an embedded anonymous field instead of a normal named field?
- Add a new perimeter method to the Shape interface to calculate the perimeter of a shape. Implement the method for Circle and Rectangle.
